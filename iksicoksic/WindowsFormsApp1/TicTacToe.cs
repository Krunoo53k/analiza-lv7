﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
	public partial class Form1 : Form
	{

		bool turn = true;
		int turn_count = 0;
		int x_win_count = 0;
		int o_win_count = 0;
		string Player_1_name = "";
		string Player_2_name = "";

		public Form1()
		{
			InitializeComponent();
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void button_click(object sender, EventArgs e)//svaki klik na gumb mijenja se potez te se mijenja igrač
		{
			Button b = (Button)sender;
			if (turn)
				b.Text = "X";
			else
				b.Text = "O";

			
			turn_count++;

			turn = !turn;
			b.Enabled = false;
			Win();
			
		}

		private void Win() // slučaji koji donose pobijedu
		{
			bool won = false;

			if ((A1.Text == A2.Text) && (A2.Text == A3.Text) && (!A1.Enabled))
				won = true;
			else if((B1.Text == B2.Text) && (B2.Text==B3.Text) && (!B1.Enabled))
				won = true;
			else if ((C1.Text == C2.Text) && (C2.Text==C3.Text) && (!C1.Enabled))
				won = true;

			else if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (!A1.Enabled))
				won = true;
			else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (!A2.Enabled))
				won = true;
			else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (!A3.Enabled))
				won = true;

			else if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (!A1.Enabled))
				won = true;
			else if ((A3.Text == B2.Text) && (B2.Text == C1.Text) && (!C1.Enabled))
				won = true;

			if (won)// ako je došlo do pobjede, završavamo igru i dodajemo pobijedu pobjedniku
			{
				end_Game();
				if (turn)
				{
					o_win_count++;
					MessageBox.Show(Player_2_name +" wins!\nWon so far: " + o_win_count + "\n\n"+ Player_1_name +  " won: " + x_win_count);
				}
				else
				{
					x_win_count++;
					MessageBox.Show(Player_1_name +" wins!\nWon so far: " + x_win_count + "\n\n"+ Player_2_name + " won: " + o_win_count);
				}
				
			}
			else {
				if (turn_count == 9) {
					MessageBox.Show("Draw!");
				}
			}

		}

		private void end_Game()
		{
			foreach (Control c in Controls) //prolazimo kroz sve buttone te ih disableamo,iznimke radimo zbog ostalih
											// stvari u cijeloj formi, poput menu stripa
			{
				try
				{
					Button b = (Button)c;
					b.Enabled = false;
				}
				catch { }
			}
		}

		private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
		{
			turn = true;
			turn_count = 0;
			foreach (Control c in Controls) // prolazimo kroz sve buttone i ponovno ih enableamo uz resetiranje reda
											// da počinje igrač sa Xom opet i resetiramo brojač reda
			{
				try
				{
					Button b = (Button)c;
					b.Enabled = true;
					b.Text = "";
				}
				catch { }
			}
		}

		private void Player1name(object sender, EventArgs e)
		{
			Player_1_name = textBox1.Text;
		}

		private void Player2name(object sender, EventArgs e)
		{
			Player_2_name = textBox2.Text;
		}
	}
}
